package org.villablanca;

public class Main {
	/**
	 * @return Devuelve la opción elegida del menú de gestión de alumnos.
	 */
	public static int menuGestionAlumnos() {
		int resultado = 0;
		do {
			System.out.println("\nSeleccione una opción:");
			System.out.println("\t1.- Dar de alta a un alumno.");
			System.out.println("\t2.- Buscar alumno por DNI.");
			System.out.println("\t3.- Borrar a un alumno.");
			System.out.println("\t4.- Modificar datos de un alumno.");
			System.out.println("\t5.- Volver atrás.");
			if (resultado < 1 || resultado > 5)
				System.out.println("Opción incorrecta.");
		} while (resultado < 1 || resultado > 4);
		return resultado;
	}

	/**
	 * @return Devuelve la opción elegida del menú de gestión de cursos.
	 */
	public static int menuGestionCursos() {
		int resultado = 0;
		do {
			System.out.println("\nSeleccione una opción:");
			System.out.println("\t1.- Dar de alta curso.");
			System.out.println("\t2.- Dar de baja curso.");
			System.out.println("\t3.- Modificar curso.");
			System.out.println("\t4.- Consultar curso.");
			System.out.println("\t5.- Volver atrás.");
			if (resultado < 1 || resultado > 5)
				System.out.println("Opción incorrecta.");
		} while (resultado < 1 || resultado > 4);
		return resultado;
	}

	/**
	 * @return Devuelve la opción elegida del menú de gestión de notas.
	 */
	public static int menuGestionNotas() {
		int resultado = 0;
		do {
			System.out.println("\nSeleccione una opción:");
			System.out.println("\t1.- Añadir notas de una asignatura.");
			System.out.println("\t2.- Consultar notas de una asignatura/curso..");
			System.out.println("\t3.- Volver atrás.");
			if (resultado < 1 || resultado > 3)
				System.out.println("Opción incorrecta.");
		} while (resultado < 1 || resultado > 2);
		return resultado;
	}

	public static void main(String[] args) {
		//Variable que indicara el numero maximo de alumnos en la academia
		int numPlazas = 30;
		int numCursos = 10;
		
		Curso[] cursos = new Curso[numCursos];
		Alumno[] alumnos = new Alumno[numPlazas];
		Nota[] notas = new Nota[numPlazas];
		

	}

}
