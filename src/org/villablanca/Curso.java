package org.villablanca;

public class Curso {
	
	private String nombreCiclo;
	private int curso;
	private Asignatura []asignaturas;
	public static final int MAX_ASIGNATURAS = 10;
	
	public Curso(String nombreCiclo, int curso) {
		this.nombreCiclo = nombreCiclo;
		this.curso = curso;
		asignaturas = new Asignatura[MAX_ASIGNATURAS];
	}
}
