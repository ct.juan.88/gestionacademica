package org.villablanca;

/**
 * 
 * @author Jonathan Toledo Recio
 *
 */
public class Alumno {
	
	
	private int nia;
	private String nombre;
	private String apellidos;
	private String dni;
	private Fecha fechaNacimiento;
	
	/**
	 * 
	 * @param nia identificador numerico del alumno
	 * @param nombre nombre del alumno
	 * @param apellidos apellidos del alumno separados de un espacio
	 * @param dni documento nacional de identidad del alumno
	 * @param fechaNacimiento objeto fecha que representa la fecha de nacimiento del alumno
	 */
	
	public Alumno (int nia, String nombre, String apellidos, String dni, Fecha fechaNacimiento) {
		this.nia = nia;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
		this.fechaNacimiento = fechaNacimiento;
	}

}
