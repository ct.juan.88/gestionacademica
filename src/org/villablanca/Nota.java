package org.villablanca;

public class Nota {
	/**
	 * 
	 * @param asignatura indica la asignatura a la que refiere la nota
	 * @param alumno indica el alumno al que refiere la nota
	 * @param notas array que almacena las diferentes notas segun la evaluacion dentro del objeto nota
	 */
	private Asignatura asignatura;
	private Alumno alumno;
	private double[]notas;
	public static final int EVALUACIONES_TOTALES = 5;
	
	public Nota(Asignatura asignatura, Alumno alumno) {
		this.asignatura = asignatura;
		this.alumno = alumno;
		this.notas = new double[EVALUACIONES_TOTALES];
	}
	public void addNota(int evaluacion, double nota) {
		notas[evaluacion - 1] = nota;
	}
	public double getNota(int evaluacion) {
		return notas[evaluacion - 1];
	}

}
